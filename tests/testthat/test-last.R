testthat::context("last: get the last of a kind")

testthat::test_that("The last is retrieved", {
    testthat::expect_equal(Collections:::last(1, 2, 3, 4, predicate = function(x) x == 2, na.rm = FALSE, null.rm = FALSE), 2)
})
