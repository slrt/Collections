testthat::context("Set: real sets, without duplicates and all")

sp1 <- function() HashSet$new(
    elements = NULL,
    hash = defaultHash,
    equals = function(x, y) x() == y())
fa <- function() "Alicia"
fb <- function() "Bobby"
fc <- function() "Cameron"
fd <- function() "Donald"
fe <- function() "Esmeralda"
ff <- function() "Franklin"
l2 <- list(fa, fb, fc, fd, fe)
sp2 <- function() HashSet$new(
    elements = l2,
    hash = defaultHash,
    equals = function(x, y) x() == y())

testthat::test_that("Set: new and asList do the job", {
    testthat::expect_equal(sp1()$asList(), list())
    testthat::expect_equal(sp2()$asList(), l2)
})

testthat::test_that("Set: get and asList do the job", {
    testthat::expect_equal(sp1()$get(fa), NA)
    testthat::expect_equal(sp2()$get(fa), fa)
    testthat::expect_equal(sp2()$get(ff), NA)
})

testthat::test_that("Set: getAll and asList do the job", {
    testthat::expect_equal(sp1()$getAll(c(fb, fd)), list())
    testthat::expect_equal(sp2()$getAll(c()), list())
    testthat::expect_equal(sp2()$getAll(list()), list())
    testthat::expect_equal(sp2()$getAll(c(fb, fd, ff)), c(fb, fd))
})

testthat::test_that("Set: put and asList do the job", {
    testthat::expect_equal(sp1()$put(fa), 1)
    s1 <- sp1()
    s1$put(fa)
    testthat::expect_equal(s1$asList(), c(fa))
    testthat::expect_equal(sp2()$put(ff), 1)
    s2 <- sp2()
    s2$put(fa)
    testthat::expect_equal(s2$asList(), l2)
    s2 <- sp2()
    s2$put(ff)
    testthat::expect_equal(s2$asList(), c(l2, ff))
})

testthat::test_that("Set: putAll and asList do the job", {
    s1 <- sp1()
    s1$putAll(l2)
    testthat::expect_equal(s1$asList(), l2)
    s1 <- sp1()
    s1$putAll(list())
    testthat::expect_equal(s1$asList(), list())
})

testthat::test_that("Set: has and asList do the job", {
    testthat::expect_false(sp1()$has(fa))
    testthat::expect_false(sp2()$has(ff))
    testthat::expect_true(sp2()$has(fa))
})

testthat::test_that("Set: hasAll and asList do the job", {
    testthat::expect_true(sp1()$hasAll(list()))
    testthat::expect_false(sp1()$hasAll(c(fa)))
    testthat::expect_false(sp2()$hasAll(c(fa, ff)))
    testthat::expect_true(sp2()$hasAll(l2))
})

testthat::test_that("Set: remove and asList do the job", {
    testthat::expect_equal(sp1()$remove(fa), NA)
    testthat::expect_equal(sp2()$remove(fa), fa)
    s2 <- sp2()
    s2$remove(fa)
    testthat::expect_equal(s2$asList(), list(fb, fc, fd, fe))
})

testthat::test_that("Set: removeAll and asList do the job", {
    testthat::expect_equal(sp1()$removeAll(l2), list())
    testthat::expect_equal(sp2()$removeAll(l2), l2)
    s2 <- sp2()
    s2$removeAll(l2)
    testthat::expect_equal(s2$asList(), list())
})

testthat::test_that("Set: size and asList do the job", {
    testthat::expect_equal(sp1()$size(), 0)
    s1 <- sp1()
    s1$put(fa)
    testthat::expect_equal(s1$size(), 1)
    testthat::expect_equal(sp2()$size(), 5)
    s2 <- sp2()
    s2$removeAll(l2)
    testthat::expect_equal(s2$size(), 0)
})

testthat::test_that("Set: iterator does the job", {
    s1 <- sp1()
    i1 <- s1$iterator()
    testthat::expect_equal(i1$currentValue(), NA)
    testthat::expect_false(i1$hasPrevious())
    testthat::expect_equal(i1$previousValue(), NA)
    testthat::expect_false(i1$hasNext())
    testthat::expect_equal(i1$nextValue(), NA)
    s2 <- sp2()
    i2 <- s2$iterator()
    testthat::expect_equal(i2$currentValue(), NA)
    testthat::expect_false(i2$hasPrevious())
    testthat::expect_equal(i2$previousValue(), NA)
    testthat::expect_true(i2$hasNext())
    testthat::expect_equal(i2$nextValue(), fa)
    testthat::expect_false(i2$hasPrevious())
    testthat::expect_true(i2$hasNext())
    testthat::expect_equal(i2$nextValue(), fb)
    testthat::expect_equal(i2$nextValue(), fc)
    testthat::expect_equal(i2$nextValue(), fd)
    testthat::expect_equal(i2$nextValue(), fe)
    testthat::expect_false(i2$hasNext())
    testthat::expect_equal(i2$nextValue(), NA)
    testthat::expect_true(i2$hasPrevious())
    testthat::expect_equal(i2$previousValue(), fe)
    s2 <- sp2()
    i2 <- s2$iterator()
    testthat::expect_equal(i2$nextValue(), fa)
    testthat::expect_equal(i2$previousValue(), NA)
    testthat::expect_equal(i2$nextValue(), fa)
})

testthat::test_that("Set: isEmpty does the job", {
    testthat::expect_true(sp1()$isEmpty())
    testthat::expect_false(sp2()$isEmpty())
})

testthat::test_that("Set: equals does the job", {
    testthat::expect_true(sp1()$equals(sp1()))
    testthat::expect_false(sp2()$equals(sp1()))
    testthat::expect_true(sp2()$equals(sp2()))
    testthat::expect_false(sp1()$equals(sp2()))
})
